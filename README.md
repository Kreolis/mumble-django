# Mumble-Django

This is <a href="" target="_blank">Mumble-Django</a>, a Murmur config application for <a href="http://www.djangoproject.com/" target="_blank">Django</a> that configures a Mumble server. It is able to create and remove server instances, start/stop them, and configure them. 

Furthermore, registered Django users can sign up for user accounts on the configured vservers and change their registration names and passwords, and Django admins can manage server admins through the webinterface.<br />

It is based on the awesome work <a href="http://bitbucket.org/Svedrin/mumble-django/" target="_blank">Mumble-Django</a> by Michael "Svedrin" Ziegler. Marius "Kreolis" Anger ported it to Python3.

# Overview

This package contains a fully functional Django project that runs as a
standalone web application. Use this if you do not have a Django-driven
website that you could integrate mumble-django in.

The main Mumble application is a complete reusable Module of its own, so
integrating it in your own Django project is fairly easy if you're familiar
with the way Django works. To do this, you will need the following
directories:
* pyweb/mumble:    The main application code
* template/mumble: The templates
* htdocs/mumble:   Static content like images and CSS for the Channel Viewer

For installation instructions, see the file "INSTALL" in this
directory.



All JavaScript elements were implemented using <a href="http://www.extjs.com/" target="_blank">the ExtJS JavaScript library.</a>
