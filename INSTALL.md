Thank you for downloading mumble-django. :-)

This application requires Python 3.x or later, and a
functional installation of Django 3.x or newer. You can obtain Python
from http://www.python.org/ and Django from http://www.djangoproject.com/.

To install mumble-django, extract it to wherever you would like it to
reside.
Install all required python3 packages by calling

    pip3 install -r base_requirements.txt

If you want to use murmur with ICE connection (recommended) 
install further python packages:

    pip3 install -r ice_requirements.txt

Please have a look at pyweb/settings.py and configure it to your needs by creating 
local_settings.py. You can add your changes thier easily and will be able to update to
the latest mumble-django version without reconfiguring everytime.
Use the normal Django settings language.

Django_Registration requires a valid EMail address to be set,
that can be used as a sender address for the registration emails. If you
intend to use the registration feature, you will need to change the
DEFAULT_FROM_EMAIL setting in local_settings.py.

Note that there it is discouraged to use the same Database that Murmur itself
uses, as mumble-django exclusively uses DBus to connect to Murmur and does not
access Murmur's database directly.

You then have to create your database and populate it with the correct tables:

    python3 manage.py migrate --run-syncdb

In case of an emergency, you can always skip this step and repeat it later
simply by running the syncdb command again.

You can test your mumble-django instance locally by running 

    python3 manage.py runserver

After that a link will be displayed. This can be accsesed via your browser.

For an empty database you want to create a superuser, that can access the administation panel.

    python3 manage.py createsuperuser

This only has to be done once at the startof your instance. All further admin and other users
can be configured within the webapp.

If you need instructions on how to configure your web server in order to
serve this application, we provide wsgi scripts as well as nginx and apache configurations 
to help you get started.
The wsgi script is also able to detect the installation paths automatically, so you
shouldn't need to edit it.

In order to use the Munin plugin that ships with Mumble-Django, just symlink
munin.py to /etc/munin/plugins and restart munin-node. Munin will then create
a new section called "Mumble users" in the Network category. Don't worry if
the numbers show up as "nan" at first, this will go away after a few Munin
runs. To test the plugin, simply run it:
    standalone: python munin.py
    via Munin:  munin-run <whatever you named the symlink in plugins>
It should give you an output like "1.value 10". If it doesn't, you might need
to set the MUMBLE_DJANGO_ROOT variable to the path you extracted Mumble-Django
to (i.e, the one mumble-django.wsgi can be found in).
