# -*- coding: utf-8 -*-
# kate: space-indent on; indent-width 4; replace-tabs on;
# Django settings for mumble_django project.

"""
 *  Copyright © 2009-2010, Michael "Svedrin" Ziegler <diese-addy@funzt-halt.net>
 *
 *  Mumble-Django is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This package is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
"""

import os

#################################################################
#################################################################
##                                                             ##
## The slice to use for communication over ZeroC Ice.          ##
## This must be set to the path to the Murmur.ice file that    ##
## resides in your Murmur directory.                           ##
## Not used on recent Murmur versions, because the slice file  ##
## can be retrieved from Murmur directly.                      ##
SLICE = '/usr/share/murmur/Murmur.ice'                          ##
##                                                             ##
## The include dir to pass to Ice. Set this to a path in which ##
## $SLICEDIR/Ice/SliceChecksumDict.ice exists.                 ##
## Not used on recent Ice versions, because the SLICEDIR can   ##
## be retrieved from Ice directly.                             ##
SLICEDIR = '/usr/share/Ice/slice'                              ##
##                                                             ##
#################################################################
##                                                             ##
##  The path inside the VirtualHost that M-D lives in:         ##
##                                                             ##
MUMBLE_DJANGO_URL  = '/'                                       ##
#MUMBLE_DJANGO_URL  = '/mumble-django/'                        ##
##                                                             ##
##  Make sure you use a trailing slash!                        ##
##                                                             ##
#################################################################
##                                                             ##
##  Mumble-Django will try to auto-detect this value if it     ##
##  isn't set, which is the default. However, if this should   ##
##  not work as expected, set this to the path where you       ##
##  extracted Mumble-Django.                                   ##
##                                                             ##
##  Default: Auto Detection                                    ##
MUMBLE_DJANGO_ROOT = None                                      ##
##  Examples:                                                  ##
#MUMBLE_DJANGO_ROOT = '/srv/mumble-django'                     ##
#MUMBLE_DJANGO_ROOT = 'c:/web/mumble-django'                   ##
##                                                             ##
#################################################################
##                                                             ##
##  For a basic installation, this is all you need to edit in  ##
##  this file, the rest will be handled automatically!         ##
##                                                             ##
#################################################################
#################################################################

# Show debug information on errors?
# If you want to file a bug report, please enable this option.
DEBUG = True
# These IPs will be delivered the debug templates if DEBUG == True.
#INTERNAL_IPS = ['127.0.0.1']

ALLOWED_HOSTS=['*']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Berlin'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# The Theme to use. None for the builtin theme.
THEME = None

# URL Template for constructing Gravatars.
GRAVATAR_URL = 'http://www.gravatar.com/avatar/%(hash)s.jpg?d=monsterid&s=%(size)d'
# Automatically set the Gravatar as the user's avatar when creating new users?
USE_GRAVATAR = False

# The default connection string to set for newly created instances.
# Ice:
DEFAULT_CONN = 'Meta:tcp -h 127.0.0.1 -p 6502'
# DBus:
#DEFAULT_CONN = 'net.sourceforge.mumble.murmur'

# Should subchannels be shown, even if there are no players in them?
SHOW_EMPTY_SUBCHANS = False

# Protect the registration form for private servers?
# If set to True, people will need to enter the server password in order to create accounts,
# and will not be able to link existing accounts.
PROTECTED_MODE = False

# Account linking allows users who registered their accounts through Mumble instead of using
# Mumble-Django, to tell MD that this account belongs to them. Then they can use MD to change
# their passwords.
# This will of course require them to enter the password that belongs to the Murmur account,
# and the accounts will only be linked if the password is correct.
# By default, this is enabled only for non-admin accounts, because if an admin account gets
# stolen they could easily take over the server. (So make sure the password can't be easily
# guessed, use at least over 9000 letters, blah blah.)
# This feature is only available if PROTECTED_MODE is not active.
ALLOW_ACCOUNT_LINKING = True          # Allow linking in general?
ALLOW_ACCOUNT_LINKING_ADMINS = False  # Allow linking for Admin accounts?

# Warning and Critical levels for the Munin plugin. These will be multiplied with the
# server instance's slot count to calculate the real levels.
MUNIN_WARNING  = 0.80
MUNIN_CRITICAL = 0.95
# The graph title.
MUNIN_TITLE    = 'Mumble Users'
# see <http://munin.projects.linpro.no/wiki/graph_category_list> for a list of valid categories.
MUNIN_CATEGORY = 'mumble'

# Set to True if you do not wish offline servers to appear in the server list.
HIDE_OFFLINE_SERVERS=False

# True to display users above channels in the channel viewer.
USERS_ABOVE_CHANNELS=False

###################################################################
##                                                               ##
##  The following settings normally do not require changes, and  ##
##  you should only change them if you know what you're doing.   ##
##                                                               ##
###################################################################

from os.path import join, dirname, abspath, exists

if not MUMBLE_DJANGO_ROOT or not exists( MUMBLE_DJANGO_ROOT ):
    MUMBLE_DJANGO_ROOT = dirname(dirname(abspath(__file__)))

BASE_DIR = MUMBLE_DJANGO_ROOT

# Default mumble port. If your server runs under this port, it will not be
# included in the links in the Channel Viewer.
MUMBLE_DEFAULT_PORT = 64738

if not MUMBLE_DJANGO_URL:
    MUMBLE_DJANGO_URL = '/'
elif MUMBLE_DJANGO_URL[-1] != '/':
    MUMBLE_DJANGO_URL = MUMBLE_DJANGO_URL + '/'

# Database settings for Mumble-Django's database. These do NOT need to point
# to Murmur's database, Mumble-Django should use its own!
DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.sqlite3',
        'NAME':     join( MUMBLE_DJANGO_ROOT, 'db', 'mumble-django.db3' ),
        'USER':     '',
        'PASSWORD': '',
        'HOST':     '',
        'PORT':     '',
        }
    }

# Email settings. only change if the defaults don't work
EMAIL_HOST = "localhost"
EMAIL_PORT = 25
EMAIL_HOST_USER = ""
EMAIL_HOST_PASSWORD = ""
#EMAIL_USE_TLS = True
# Default email address to send mails from.
DEFAULT_FROM_EMAIL = "webmaster@localhost"


SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True
USE_L10N = True

STATIC_URL = MUMBLE_DJANGO_URL + 'static/'

STATIC_ROOT = join( MUMBLE_DJANGO_ROOT, 'htdocs' )

## URL to static files of the currently active theme
THEME_URL = '%sstatic/themes/%s/' % ( MUMBLE_DJANGO_URL, THEME )

# URL to the login view
LOGIN_URL = MUMBLE_DJANGO_URL + 'accounts/login/'
LOGIN_REDIRECT_URL = MUMBLE_DJANGO_URL + 'accounts/profile/'


# Automatically generate a .secret.txt file containing the SECRET_KEY.
# Shamelessly stolen from ByteFlow: <http://www.byteflow.su>
try:
    SECRET_KEY
except NameError:
    # secret file location
    SECRET_FILE = join(MUMBLE_DJANGO_ROOT, '../.secret.txt')
    try:
        # check for already defined key
        with open(SECRET_FILE,'w') as secretFile:
            SECRET_KEY = secretFile.read().strip()
    except IOError:
        # make new key file if none available
        try:
            from django.core.management import utils
            SECRET_KEY = utils.get_random_secret_key()
            with open(SECRET_FILE,'w') as newSecretFile:
                newSecretFile.write(SECRET_KEY)
        except IOError:
            Exception('Please create a %s file with random characters to generate your secret key!' % SECRET_FILE)


MIDDLEWARE = (
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(os.path.dirname(__file__), 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'processors.installed_apps',
                'processors.version',
                'processors.mumble_media_prefix',
                #'processors.theme_url', # add only if not default theme
            ],
        },
    },
]


if THEME:
	TEMPLATE_DIRS = [ join( MUMBLE_DJANGO_ROOT, 'themes', THEME ) ]
else:
	TEMPLATE_DIRS = []

TEMPLATE_DIRS.extend((
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    join( MUMBLE_DJANGO_ROOT, 'pyweb', 'templates' ),
))

#TEST_RUNNER = 'mumble.testrunner.run_tests'
#TEST_MURMUR_LAB_DIR   = join( dirname(MUMBLE_DJANGO_ROOT), 'murmur' )
#TEST_MURMUR_FILES_DIR = join( MUMBLE_DJANGO_ROOT, 'testdata' )

CONVERSIONSQL_ROOT = join( MUMBLE_DJANGO_ROOT, "pyweb", "mumble", "conversionsql" )

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sessions',
    'django.contrib.sites',
    'mumble',
]

AUTHENTICATION_BACKENDS = [
    
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',
]


# Length of the account activation period, in days.
ACCOUNT_ACTIVATION_DAYS = 2


def modprobe( name ):
    """ Try to import the named module, and if that works add it to INSTALLED_APPS. """
    try:
        __import__( name )
    except ImportError:
        pass
    else:
        INSTALLED_APPS.append( name )

# Check if rosetta is available.
#    http://code.google.com/p/django-rosetta
modprobe( "rosetta" )

# Check if django_extensions is available.
modprobe( "django_extensions" )

# Check if django_registration is available.
modprobe( "allauth" )
#modprobe( "django_registration" )


if 'allauth' in INSTALLED_APPS:
    INSTALLED_APPS.append('allauth.account')
    INSTALLED_APPS.append('allauth.socialaccount')

    # `allauth` specific authentication methods, such as login by e-mail
    AUTHENTICATION_BACKENDS.append('allauth.account.auth_backends.AuthenticationBackend')

    try:
        # enable or disable services in this file: local_allauth_settings.py
        from local_allauth_settings import socialAccountSettings
        print("\nUsing local allauth settings!\n")
        for setting in socialAccountSettings:
            INSTALLED_APPS.append(setting)

        '''
        Ṕrocvide in local_allauth_socialaccounts.py the apps for the services 
        you enabled in local_allauth_settings in the following format.
        See https://django-allauth.readthedocs.io/en/latest/providers.html
        for more details on how to configure them

        SOCIALACCOUNT_PROVIDERS = {
            'AUTH_NAME': {
                # For each OAuth based provider, either add a ``SocialApp``
                # (``socialaccount`` app) containing the required client
                # credentials, or list them here:
                'APP': {
                    'client_id': '',
                    'secret': '',
                    'key': ''
                }
            }
        }
        '''
        from local_allauth_socialaccounts import SOCIALACCOUNT_PROVIDERS
        
    except ImportError as e:
        print("\nNo local allauth settings found: ", e, "\n Using defaults!\n")

try:
    '''
    You can provide your own local (e.g. production) settings in local_settings.py.
    Otherwise it will use defaults.
    Use them same syntax and names as in normal Django settings file.
    '''
    from local_settings import *
    print("\nUsing local settings!\n")
except ImportError as e:
    print("\nNo local settings found: ", e, "\n Using defaults!\n")

TEMPLATE_DEBUG = DEBUG