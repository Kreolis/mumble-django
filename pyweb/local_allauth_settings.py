
socialAccountSettings = []
# ... include the providers you want to enable:
# more are available. see here for more information:
# https://django-allauth.readthedocs.io/en/latest/providers.html
#socialAccountSettings.append('allauth.socialaccount.providers.auth0')
#socialAccountSettings.append('allauth.socialaccount.providers.authentiq')
#socialAccountSettings.append('allauth.socialaccount.providers.bitbucket')
#socialAccountSettings.append('allauth.socialaccount.providers.bitbucket_oauth2')
#socialAccountSettings.append('allauth.socialaccount.providers.cern')
socialAccountSettings.append('allauth.socialaccount.providers.discord')
socialAccountSettings.append('allauth.socialaccount.providers.eveonline')
#socialAccountSettings.append('allauth.socialaccount.providers.github')
#socialAccountSettings.append('allauth.socialaccount.providers.gitlab')
#socialAccountSettings.append('allauth.socialaccount.providers.google')
#socialAccountSettings.append('allauth.socialaccount.providers.nextcloud')
#socialAccountSettings.append('allauth.socialaccount.providers.patreon')
#socialAccountSettings.append('allauth.socialaccount.providers.reddit')
#socialAccountSettings.append('allauth.socialaccount.providers.stackexchange')
#socialAccountSettings.append('allauth.socialaccount.providers.steam')
#socialAccountSettings.append('allauth.socialaccount.providers.twitch')
#socialAccountSettings.append('allauth.socialaccount.providers.vimeo')
#socialAccountSettings.append('allauth.socialaccount.providers.vimeo_oauth2')