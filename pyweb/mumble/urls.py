# -*- coding: utf-8 -*-
# kate: space-indent on; indent-width 4; replace-tabs on;

"""
 *  Copyright © 2009-2010, Michael "Svedrin" Ziegler <diese-addy@funzt-halt.net>
 *
 *  Mumble-Django is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This package is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
"""

from django.urls import include, path, re_path
from django.conf import settings
from django.views.static import serve

from . import views, forms

from .views import EXT_DIRECT_PROVIDER
from .forms import EXT_FORMS_PROVIDER

urlpatterns = [

    path('api/',   include(EXT_DIRECT_PROVIDER.urls) , name='mumbleApi'),
    path('forms/', include(EXT_FORMS_PROVIDER.urls)  , name='mumbleForms'),

    re_path(r'(?P<server>\d+)/(?P<userid>\d+)/texture.png',    views.showTexture, name='serverUserTexture' ),
    re_path(r'(?P<userid>\d+)/update_avatar',      views.update_avatar , name='mumbleUpdateAvatar' ),

    re_path(r'murmur/tree/(?P<server>\d+)',        views.mmng_tree, name='murmurTree'),
    re_path(r'mumbleviewer/(?P<server>\d+).xml',   views.mumbleviewer_tree_xml, name='serverViewerTreeXML'),
    re_path(r'mumbleviewer/(?P<server>\d+).json',  views.mumbleviewer_tree_json, name='serverViewerTreeJSON'),

    re_path(r'mobile/(?P<server>\d+)/?',          views.mobile_show, name='serverMobileShow'),
    path('mobile/?',                          views.mobile_mumbles, name='serverMobiles'),

    re_path(r'embed/(?P<server>\d+)/?',           views.embed, name='serverEmbeded'),
    re_path(r'qrcode/(?P<server>\d+).png',        views.qrcode, name='serverQrcode'),

    re_path(r'(?P<server>\d+).json',               views.cvp_json, name='servercvpJSON'),
    re_path(r'(?P<server>\d+).xml',                views.cvp_xml, name='servercvpXML'),

    re_path(r'(?P<server>\d+)/?',                 views.show, name='server'),
    re_path(r'$',                                  views.mumbles, name='mumbles'),
]

if settings.DEBUG or True:
    urlpatterns.append(
        re_path(r'^media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }))
