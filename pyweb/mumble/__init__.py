# -*- coding: utf-8 -*-
# kate: space-indent on; indent-width 4; replace-tabs on;
"""
 *  Copyright © 2009-2010, Michael "Svedrin" Ziegler <diese-addy@funzt-halt.net>
 *
 *  Mumble-Django is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This package is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
"""

current_version = { 'major': 3, 'minor': 00, 'beta': 1 }

if current_version['beta']:
    version_str = "%(major)d.%(minor)dbeta%(beta)d" % current_version
else:
    version_str = "%(major)d.%(minor)d" % current_version

def getVersions():
    """ Generator that yields all available upstream versions. """
    url = 'http://bitbucket.org/Svedrin/mumble-django/raw/tip/.hgtags'
    from urllib.request import urlopen

    with urlopen(url) as webtags:
        for line in webtags:
            line = webtags.readline().strip()
            version = line.decode('utf8').strip('\n').split(' ')
            yield version


def getLatestUpstreamVersion():
    """ Return the latest version available upstream. """
    return max(getVersions())[1].strip('v')

def isUptodate():
    """ Check if this version of Mumble-Django is the latest available. """
    return version_str >= getLatestUpstreamVersion()
