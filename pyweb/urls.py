# -*- coding: utf-8 -*-
# kate: space-indent on; indent-width 4; replace-tabs on;

"""
 *  Copyright © 2009-2010, Michael "Svedrin" Ziegler <diese-addy@funzt-halt.net>
 *
 *  Mumble-Django is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This package is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
"""

from django.urls import include, path, re_path
from django.contrib import admin
from django.conf import settings
from django.views.static import serve
from django.views.i18n import JavaScriptCatalog

import views
from mumble.views import redir

urlpatterns = [
    re_path(r'^/?$', redir, name='mumble_redir'),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs'
    # to INSTALLED_APPS to enable admin documentation:
    # path('admin/doc/', django.contrib.admindocs.urls),

    #path('accounts/', include('django.contrib.auth.urls')),

    path('accounts/profile/', views.profile, name='profile'),
    path('accounts/imprint/', views.imprint, name='imprint'),

    path('mumble/', include('mumble.urls')),

    # Uncomment the next line to enable the admin:
    path('admin/', admin.site.urls),

    path('i18n/', include('django.conf.urls.i18n')),
    re_path(r'jsi18n/$',
         JavaScriptCatalog.as_view(packages=['mumble']),
         name='javascript-catalog'),
]

if "django_registration" in settings.INSTALLED_APPS:
    urlpatterns.append(path('accounts/', include('django_registration.backends.activation.urls')))
    urlpatterns.append(path('accounts/', include('django.contrib.auth.urls')))

if "rosetta" in settings.INSTALLED_APPS:
    urlpatterns.append(path('rosetta/', include('rosetta.urls')))

if "allauth" in settings.INSTALLED_APPS:
    urlpatterns.append(path('accounts/', include('allauth.urls')))

# Development stuff
if settings.DEBUG or True:
    urlpatterns.append(re_path(r'^media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }))
